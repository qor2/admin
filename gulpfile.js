'use strict';

const { src, dest, series, parallel } = require('gulp');

const sass = require('gulp-sass');
sass.compiler = require('node-sass');

const mergeCSS = require('gulp-merge-css');
const order = require('gulp-order');
const rm = require('gulp-rm')

function compileScss(cb) {
  return src('views/assets/stylesheets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(dest('views/assets/stylesheets/'))
  cb();
}

function compileVendors(cb) {
  return src('views/assets/stylesheets/vendors/*.min.css')
    .pipe(mergeCSS({ name: 'vendors.css' }))
    .pipe(dest('views/assets/stylesheets/'))
  cb();
}

function combineCSS(cb) {
  return src('views/assets/stylesheets/*.css')
    .pipe(order([
      "views/assets/stylesheets/vendors.css",
      "views/assets/stylesheets/qor.css",
      "views/assets/stylesheets/app.css"
    ], { base: './' })
    )
    .pipe(mergeCSS({ name: 'qor_admin_default.css' }))
    .pipe(dest('views/assets/stylesheets/'))
  cb();
}

function clean(cb) {
  src('views/assets/stylesheets/*.css', { read: false })
    .pipe(rm())
  cb()
}

const compile = parallel(compileScss, compileVendors);

exports.default = series(clean, compile, combineCSS);
